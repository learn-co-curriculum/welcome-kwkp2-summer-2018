## Welcome to Kode with Klossy Part 2!

We have an exciting course ahead for you! Over the next few weeks, you will learn the fundamentals required to manipulate the most powerful tool humans have today: the Internet. Perhaps you could also consider it the most important invention of all time, but it has tough contenders amongst peers such as debt financing, agriculture, the scientific method, and (of course) computing.

There exist several core competencies that web programmers use when writing front and backend applications and we will address them in the following order:
  - JavaScript Fundamentals
  - DOM Manipulation
  - Application Programming Interfaces (APIs)
  - Using External Libraries

##### JavaScript Fundamentals

First and foremost, we are going to do a crash course in JavaScript, the language of the web. You may see the term 'ECMAScript' or 'ES' used at times. For our purposes, its safe to assume they are synonymous with JavaScript. All modern browsers run JavaScript and, since the advent of [Node][node], it has gained significant popularity as a 'backend' or 'server-side' language.

##### DOM Manipulation

As you know, JavaScript is one of the trinity that create front-end (a.k.a. in browser) experiences. HTML and CSS provide the other two layers. The DOM, or [Document-Object-Model][dom] provides the structure and medium for us to manipulate a web-page. The DOM bridges the gap between the _presentation_ of the web page and our JavaScript's ability to manipulate it.

##### Application Programming Interfaces (APIs)

There are broad definitions, and applications, of [APIs][APIs]. We are going to be exploring them in a specific use case: as a tool for accessing another application's _resources_. When we say 'API' throughout this course, we are referencing 'Web APIs', which is a fancy term for an application that allow you, as a web programmer, to engage with it programmatically. This could be anything from requesting images of cats from a database someone is exposing to the internet to passing text to a [Google API][google-api] that analyses the sentiment of some tweet's content.

##### Using External Libraries

As programmers, we are a part of a massive (and growing!) community of other talented individuals who have shared their code with the world. We can accomplish significant tasks by building on tools and frameworks that others have already made. This accelerates our productivity by allowing us to build on existing functionality. We work on leveraging two (very fun!) libraries in particular: [p5][p5] and [A-Frame][a-frame].
  - **p5**: among other things, makes working with [HTML canvas][html-canvas] easier and quicker
  - **A-Frame**: is a _robust_ framework (built itself on [another framework][three-js]) geared towards rapid development of virtual reality, or 3d immersive, experiences

## Looking Forward

Throughout the course, we encourage you to work with your peers as much as possible. It is important to note that programming is as much an individual technical skillset as it is a team working skillset. Embrace the role of a co-worker, teacher, and student with your peers as you are going through this course and you will benefit from it.

Let's get started!

[three-js]: https://threejs.org/
[html-canvas]: https://www.w3schools.com/graphics/canvas_intro.asp
[p5]: https://p5js.org/
[a-frame]: https://aframe.io/
[google-api]: https://cloud.google.com/natural-language/docs/reference/rest/v1/Sentiment
[APIs]: https://en.wikipedia.org/wiki/Application_programming_interface
[DOM]: https://developer.mozilla.org/en-US/docs/Web/API/Document_Object_Model/Introduction
[node]: https://nodejs.org/en/
